use dbtrucoweb
-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: dbtrucoweb
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `centroideQuemFlor`
--

DROP TABLE IF EXISTS `centroideQuemFlor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroideQuemFlor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo` int(11) DEFAULT NULL,
  `quemPediuFlor` double DEFAULT NULL,
  `quemPediuContraFlor` double DEFAULT NULL,
  `quemPediuContraFlorEoResto` double DEFAULT NULL,
  `numerok` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroideQuemFlor`
--

LOCK TABLES `centroideQuemFlor` WRITE;
/*!40000 ALTER TABLE `centroideQuemFlor` DISABLE KEYS */;
/*!40000 ALTER TABLE `centroideQuemFlor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidequemflor`
--

DROP TABLE IF EXISTS `centroidequemflor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidequemflor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo` int(11) DEFAULT NULL,
  `quemPediuFlor` double DEFAULT NULL,
  `quemPediuContraFlor` double DEFAULT NULL,
  `quemPediuContraFlorEoResto` double DEFAULT NULL,
  `numerok` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidequemflor`
--

LOCK TABLES `centroidequemflor` WRITE;
/*!40000 ALTER TABLE `centroidequemflor` DISABLE KEYS */;
/*!40000 ALTER TABLE `centroidequemflor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesPrimeiraCartaRoboEmao`
--

DROP TABLE IF EXISTS `centroidesPrimeiraCartaRoboEmao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesPrimeiraCartaRoboEmao` (
  `grupo` int(11) NOT NULL,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  `primeiraCartaRoboClustering` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesPrimeiraCartaRoboEmao`
--

LOCK TABLES `centroidesPrimeiraCartaRoboEmao` WRITE;
/*!40000 ALTER TABLE `centroidesPrimeiraCartaRoboEmao` DISABLE KEYS */;
INSERT INTO `centroidesPrimeiraCartaRoboEmao` VALUES (1,37.441281,21.373665,6.790036,4.359431,4,132),(2,46.614035,6.380117,3.631579,7.48538,4,133),(3,13.734024,5.982729,3.697755,4.01209,4,134),(4,14.996633,6.474747,3.666667,46,4,135);
/*!40000 ALTER TABLE `centroidesPrimeiraCartaRoboEmao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesPrimeiraCartaRoboEpe`
--

DROP TABLE IF EXISTS `centroidesPrimeiraCartaRoboEpe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesPrimeiraCartaRoboEpe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double DEFAULT NULL,
  `cartaMediaRoboClustering` double DEFAULT NULL,
  `cartaBaixaRoboClustering` double DEFAULT NULL,
  `primeiraCartaRoboClustering` double DEFAULT NULL,
  `primeiraCartaHumanoClustering` double DEFAULT NULL,
  `grupo` int(11) DEFAULT NULL,
  `numerok` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=354 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesPrimeiraCartaRoboEpe`
--

LOCK TABLES `centroidesPrimeiraCartaRoboEpe` WRITE;
/*!40000 ALTER TABLE `centroidesPrimeiraCartaRoboEpe` DISABLE KEYS */;
INSERT INTO `centroidesPrimeiraCartaRoboEpe` VALUES (347,48.727273,42.818182,6.454545,-10.045455,-3.045455,1,7),(348,16.849515,5.495146,3.504854,46,9.451456,2,7),(349,22.271429,8.2,4.057143,-39.971429,-46,3,7),(350,42.090909,26.030303,18.787879,-4.242424,11.030303,4,7),(351,17.914286,13.091429,6.462857,5.12,8.908571,5,7),(352,12.842205,5.958175,3,4.992395,8.678707,6,7),(353,45.812287,12.242321,4.351536,8.860068,9.167235,7,7);
/*!40000 ALTER TABLE `centroidesPrimeiraCartaRoboEpe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesQuemGanhouEnvidoAgenteEmao`
--

DROP TABLE IF EXISTS `centroidesQuemGanhouEnvidoAgenteEmao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesQuemGanhouEnvidoAgenteEmao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo` double NOT NULL,
  `quemPediuEnvido` double NOT NULL,
  `quemPediuRealEnvido` double NOT NULL,
  `quemPediuFaltaEnvido` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `pontosEnvidoRobo` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=498 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesQuemGanhouEnvidoAgenteEmao`
--

LOCK TABLES `centroidesQuemGanhouEnvidoAgenteEmao` WRITE;
/*!40000 ALTER TABLE `centroidesQuemGanhouEnvidoAgenteEmao` DISABLE KEYS */;
INSERT INTO `centroidesQuemGanhouEnvidoAgenteEmao` VALUES (494,1,1.003096,2.052632,2,4,26.331269),(495,2,2.232258,1.922581,2.006452,4,23.077419),(496,3,1.769231,2.230769,1,4,30.230769),(497,4,2.070866,2.010499,2.005249,4,4.879265);
/*!40000 ALTER TABLE `centroidesQuemGanhouEnvidoAgenteEmao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesQuemGanhouEnvidoAgentePe`
--

DROP TABLE IF EXISTS `centroidesQuemGanhouEnvidoAgentePe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesQuemGanhouEnvidoAgentePe` (
  `grupo` int(11) NOT NULL,
  `quemPediuEnvido` double NOT NULL,
  `quemPediuRealEnvido` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quemPediuFaltaEnvido` double NOT NULL,
  `pontosEnvidoRobo` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=472 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesQuemGanhouEnvidoAgentePe`
--

LOCK TABLES `centroidesQuemGanhouEnvidoAgentePe` WRITE;
/*!40000 ALTER TABLE `centroidesQuemGanhouEnvidoAgentePe` DISABLE KEYS */;
INSERT INTO `centroidesQuemGanhouEnvidoAgentePe` VALUES (1,2.829493,2.101382,5,467,2.069124,5.096774),(2,1,2.014563,5,468,2,26.441748),(3,2.399038,2.038462,5,469,1.942308,23.673077),(4,2.707317,1,5,470,2.073171,28.804878),(5,1.921196,2,5,471,2,5.078804);
/*!40000 ALTER TABLE `centroidesQuemGanhouEnvidoAgentePe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesQuemTrucoPrimeiraMao`
--

DROP TABLE IF EXISTS `centroidesQuemTrucoPrimeiraMao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesQuemTrucoPrimeiraMao` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesQuemTrucoPrimeiraMao`
--

LOCK TABLES `centroidesQuemTrucoPrimeiraMao` WRITE;
/*!40000 ALTER TABLE `centroidesQuemTrucoPrimeiraMao` DISABLE KEYS */;
INSERT INTO `centroidesQuemTrucoPrimeiraMao` VALUES (1,2.433862,1.906526,2.049383,2,103,18.059965,6.216931,3.634921),(2,1.084211,2.326316,1.9,2,104,34.210526,12.684211,5.7);
/*!40000 ALTER TABLE `centroidesQuemTrucoPrimeiraMao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesQuemTrucoPrimeiraPe`
--

DROP TABLE IF EXISTS `centroidesQuemTrucoPrimeiraPe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesQuemTrucoPrimeiraPe` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesQuemTrucoPrimeiraPe`
--

LOCK TABLES `centroidesQuemTrucoPrimeiraPe` WRITE;
/*!40000 ALTER TABLE `centroidesQuemTrucoPrimeiraPe` DISABLE KEYS */;
INSERT INTO `centroidesQuemTrucoPrimeiraPe` VALUES (1,1,3,1,5,166,38.243243,16.837838,6.378378),(2,2.021277,1.489362,2.446809,5,167,38.382979,20.489362,11.382979),(3,1.208333,2.135417,2,5,168,35.614583,11.067708,5.0625),(4,3,1.854369,2.006472,5,169,19.061489,6.126214,3.569579),(5,1.78169,2.014085,2,5,170,13.919014,5.96831,3.450704);
/*!40000 ALTER TABLE `centroidesQuemTrucoPrimeiraPe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesQuemTrucoSegundaGanhouAnterior`
--

DROP TABLE IF EXISTS `centroidesQuemTrucoSegundaGanhouAnterior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesQuemTrucoSegundaGanhouAnterior` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=282 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesQuemTrucoSegundaGanhouAnterior`
--

LOCK TABLES `centroidesQuemTrucoSegundaGanhouAnterior` WRITE;
/*!40000 ALTER TABLE `centroidesQuemTrucoSegundaGanhouAnterior` DISABLE KEYS */;
INSERT INTO `centroidesQuemTrucoSegundaGanhouAnterior` VALUES (1,1.702265,2.006472,2,4,278,16.860841,6.909385,4.135922),(2,3,1.754717,2.067925,4,279,21.535849,6.973585,3.845283),(3,1.166667,2.275862,1.810345,4,280,39.609195,12.936782,5.103448),(4,1.136364,2.113636,1.818182,4,281,42.659091,28.295455,14.863636);
/*!40000 ALTER TABLE `centroidesQuemTrucoSegundaGanhouAnterior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesQuemTrucoSegundaPerdeuAnterior`
--

DROP TABLE IF EXISTS `centroidesQuemTrucoSegundaPerdeuAnterior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesQuemTrucoSegundaPerdeuAnterior` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesQuemTrucoSegundaPerdeuAnterior`
--

LOCK TABLES `centroidesQuemTrucoSegundaPerdeuAnterior` WRITE;
/*!40000 ALTER TABLE `centroidesQuemTrucoSegundaPerdeuAnterior` DISABLE KEYS */;
INSERT INTO `centroidesQuemTrucoSegundaPerdeuAnterior` VALUES (1,3,1.831746,2.101587,3,239,20.209524,6.863492,3.761905),(2,1.865385,2,2,3,240,13.676282,5.160256,3.24359),(3,1.065868,2.419162,1.91018,3,241,34.269461,10.419162,4.754491);
/*!40000 ALTER TABLE `centroidesQuemTrucoSegundaPerdeuAnterior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesQuemTrucoTerceiraGanhouAnterior`
--

DROP TABLE IF EXISTS `centroidesQuemTrucoTerceiraGanhouAnterior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesQuemTrucoTerceiraGanhouAnterior` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesQuemTrucoTerceiraGanhouAnterior`
--

LOCK TABLES `centroidesQuemTrucoTerceiraGanhouAnterior` WRITE;
/*!40000 ALTER TABLE `centroidesQuemTrucoTerceiraGanhouAnterior` DISABLE KEYS */;
INSERT INTO `centroidesQuemTrucoTerceiraGanhouAnterior` VALUES (1,2.315152,2,2,7,263,15.4,6.724242,3.981818),(2,1,3,1.753425,7,264,27.739726,9.753425,4.164384),(3,3,1.557377,2,7,265,41.196721,8,4.180328),(4,1.306748,2,2,7,266,45.785276,10.392638,4.674847),(5,3,1,3,7,267,30.138889,12.861111,5),(6,1.016,2,2,7,268,18.72,10.64,4.72),(7,1.139535,1.976744,1.953488,7,269,44.139535,27.27907,15.139535);
/*!40000 ALTER TABLE `centroidesQuemTrucoTerceiraGanhouAnterior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesQuemTrucoTerceiraPerdeuAnterior`
--

DROP TABLE IF EXISTS `centroidesQuemTrucoTerceiraPerdeuAnterior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesQuemTrucoTerceiraPerdeuAnterior` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesQuemTrucoTerceiraPerdeuAnterior`
--

LOCK TABLES `centroidesQuemTrucoTerceiraPerdeuAnterior` WRITE;
/*!40000 ALTER TABLE `centroidesQuemTrucoTerceiraPerdeuAnterior` DISABLE KEYS */;
INSERT INTO `centroidesQuemTrucoTerceiraPerdeuAnterior` VALUES (1,3,1.855643,2.036745,3,253,18.136483,6.044619,3.48294),(2,1.727794,2.048711,2,3,254,15.896848,6.091691,3.644699),(3,1,3,1,3,255,39.916667,16.527778,6.333333);
/*!40000 ALTER TABLE `centroidesQuemTrucoTerceiraPerdeuAnterior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesSegundaCartaRoboGanhouAprimeira`
--

DROP TABLE IF EXISTS `centroidesSegundaCartaRoboGanhouAprimeira`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesSegundaCartaRoboGanhouAprimeira` (
  `grupo` int(11) NOT NULL,
  `segundaCartaRoboClustering` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesSegundaCartaRoboGanhouAprimeira`
--

LOCK TABLES `centroidesSegundaCartaRoboGanhouAprimeira` WRITE;
/*!40000 ALTER TABLE `centroidesSegundaCartaRoboGanhouAprimeira` DISABLE KEYS */;
INSERT INTO `centroidesSegundaCartaRoboGanhouAprimeira` VALUES (1,-5.166667,3,142),(2,11.634529,3,143),(3,-46,3,144);
/*!40000 ALTER TABLE `centroidesSegundaCartaRoboGanhouAprimeira` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesSegundaCartaRoboPerdeuAprimeira`
--

DROP TABLE IF EXISTS `centroidesSegundaCartaRoboPerdeuAprimeira`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesSegundaCartaRoboPerdeuAprimeira` (
  `grupo` int(11) NOT NULL,
  `segundaCartaHumanoClustering` double NOT NULL,
  `segundaCartaRoboClustering` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=334 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesSegundaCartaRoboPerdeuAprimeira`
--

LOCK TABLES `centroidesSegundaCartaRoboPerdeuAprimeira` WRITE;
/*!40000 ALTER TABLE `centroidesSegundaCartaRoboPerdeuAprimeira` DISABLE KEYS */;
INSERT INTO `centroidesSegundaCartaRoboPerdeuAprimeira` VALUES (1,-46,-16.849624,7,327),(2,46.945946,-2.567568,7,328),(3,7.2,-14.888,7,329),(4,7.66879,28.850318,7,330),(5,4.065574,-46,7,331),(6,-13.394737,-15,7,332),(7,20,-14.77381,7,333);
/*!40000 ALTER TABLE `centroidesSegundaCartaRoboPerdeuAprimeira` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesTerceiraCartaRoboGanhouAsegunda`
--

DROP TABLE IF EXISTS `centroidesTerceiraCartaRoboGanhouAsegunda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesTerceiraCartaRoboGanhouAsegunda` (
  `grupo` int(11) NOT NULL,
  `terceiraCartaRoboClustering` double NOT NULL,
  `numerok` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tereiraCartaRoboClustering` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesTerceiraCartaRoboGanhouAsegunda`
--

LOCK TABLES `centroidesTerceiraCartaRoboGanhouAsegunda` WRITE;
/*!40000 ALTER TABLE `centroidesTerceiraCartaRoboGanhouAsegunda` DISABLE KEYS */;
INSERT INTO `centroidesTerceiraCartaRoboGanhouAsegunda` VALUES (1,46,4,201,NULL),(2,-46,4,202,NULL),(3,-14.8,4,203,NULL),(4,13.061224,4,204,NULL);
/*!40000 ALTER TABLE `centroidesTerceiraCartaRoboGanhouAsegunda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesTerceiraCartaRoboPerdeuAsegunda`
--

DROP TABLE IF EXISTS `centroidesTerceiraCartaRoboPerdeuAsegunda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesTerceiraCartaRoboPerdeuAsegunda` (
  `grupo` int(11) NOT NULL,
  `terceiraCartaHumanoClustering` double NOT NULL,
  `terceiraCartaRoboClustering` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesTerceiraCartaRoboPerdeuAsegunda`
--

LOCK TABLES `centroidesTerceiraCartaRoboPerdeuAsegunda` WRITE;
/*!40000 ALTER TABLE `centroidesTerceiraCartaRoboPerdeuAsegunda` DISABLE KEYS */;
INSERT INTO `centroidesTerceiraCartaRoboPerdeuAsegunda` VALUES (1,13.045662,40.657534,4,157),(2,24.298246,-12.397661,4,158),(3,-21.965517,-46,4,159),(4,-14.133188,-13.508734,4,160);
/*!40000 ALTER TABLE `centroidesTerceiraCartaRoboPerdeuAsegunda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesgrupoindexacao`
--

DROP TABLE IF EXISTS `centroidesgrupoindexacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesgrupoindexacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `centroidecartaaltarobomao` double NOT NULL,
  `centroidecartamediarobomao` double NOT NULL,
  `centroidecartabaixarobomao` double NOT NULL,
  `grupo` int(11) NOT NULL,
  `numerok` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesgrupoindexacao`
--

LOCK TABLES `centroidesgrupoindexacao` WRITE;
/*!40000 ALTER TABLE `centroidesgrupoindexacao` DISABLE KEYS */;
INSERT INTO `centroidesgrupoindexacao` VALUES (274,24.694215,9.830579,8.561983,1,3),(275,18.648767,5.652055,3,2,3),(276,37.881508,21.536804,5.477558,3,3);
/*!40000 ALTER TABLE `centroidesgrupoindexacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesgrupoindexacaopontosEnvidoRobo`
--

DROP TABLE IF EXISTS `centroidesgrupoindexacaopontosEnvidoRobo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesgrupoindexacaopontosEnvidoRobo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `centroidePontosEnvidoRobo` double NOT NULL,
  `grupo` int(11) NOT NULL,
  `numerok` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grupo_UNIQUE` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesgrupoindexacaopontosEnvidoRobo`
--

LOCK TABLES `centroidesgrupoindexacaopontosEnvidoRobo` WRITE;
/*!40000 ALTER TABLE `centroidesgrupoindexacaopontosEnvidoRobo` DISABLE KEYS */;
INSERT INTO `centroidesgrupoindexacaopontosEnvidoRobo` VALUES (127,4.985353,1,2),(128,25.913449,2,2);
/*!40000 ALTER TABLE `centroidesgrupoindexacaopontosEnvidoRobo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesgrupoindexacaopontosenvidorobo`
--

DROP TABLE IF EXISTS `centroidesgrupoindexacaopontosenvidorobo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesgrupoindexacaopontosenvidorobo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `centroidePontosEnvidoRobo` double NOT NULL,
  `grupo` int(11) NOT NULL,
  `numerok` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesgrupoindexacaopontosenvidorobo`
--

LOCK TABLES `centroidesgrupoindexacaopontosenvidorobo` WRITE;
/*!40000 ALTER TABLE `centroidesgrupoindexacaopontosenvidorobo` DISABLE KEYS */;
INSERT INTO `centroidesgrupoindexacaopontosenvidorobo` VALUES (115,25.913449,1,2),(116,4.973788,2,2);
/*!40000 ALTER TABLE `centroidesgrupoindexacaopontosenvidorobo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesprimeiracartaroboemao`
--

DROP TABLE IF EXISTS `centroidesprimeiracartaroboemao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesprimeiracartaroboemao` (
  `grupo` int(11) NOT NULL,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  `primeiraCartaRoboClustering` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesprimeiracartaroboemao`
--

LOCK TABLES `centroidesprimeiracartaroboemao` WRITE;
/*!40000 ALTER TABLE `centroidesprimeiracartaroboemao` DISABLE KEYS */;
INSERT INTO `centroidesprimeiracartaroboemao` VALUES (1,36.557692,21.192308,15.846154,8.980769,6,122),(2,14.467105,5.726974,3.578947,46,6,123),(3,13.501672,5.792642,3.615385,4.37291,6,124),(4,46.425532,6.62766,3.787234,7.808511,6,125),(5,47.133333,22.373333,4.92,4.066667,6,126),(6,20,18.550725,5.492754,10.217391,6,127);
/*!40000 ALTER TABLE `centroidesprimeiracartaroboemao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesprimeiracartaroboepe`
--

DROP TABLE IF EXISTS `centroidesprimeiracartaroboepe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesprimeiracartaroboepe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double DEFAULT NULL,
  `cartaMediaRoboClustering` double DEFAULT NULL,
  `cartaBaixaRoboClustering` double DEFAULT NULL,
  `primeiraCartaRoboClustering` double DEFAULT NULL,
  `primeiraCartaHumanoClustering` double DEFAULT NULL,
  `grupo` int(11) DEFAULT NULL,
  `numerok` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=340 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesprimeiracartaroboepe`
--

LOCK TABLES `centroidesprimeiracartaroboepe` WRITE;
/*!40000 ALTER TABLE `centroidesprimeiracartaroboepe` DISABLE KEYS */;
INSERT INTO `centroidesprimeiracartaroboepe` VALUES (332,31.7,11.333333,4.333333,-44.966667,8.9,1,8),(333,19.955056,7.539326,3.831461,-34.640449,-39.382022,2,8),(334,37.407407,20,4.358025,14.469136,9.95679,3,8),(335,48.727273,42.818182,6.454545,-10.045455,-3.045455,4,8),(336,13.404878,5.809756,3.580488,15.623171,9.264634,5,8),(337,45.684211,7.015789,3.989474,12.684211,9.052632,6,8),(338,21.855556,17.333333,7.666667,9.388889,9.155556,7,8),(339,39.2,24.422222,20,-2.044444,9.333333,8,8);
/*!40000 ALTER TABLE `centroidesprimeiracartaroboepe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesquandotruco`
--

DROP TABLE IF EXISTS `centroidesquandotruco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesquandotruco` (
  `grupo` int(11) NOT NULL,
  `quandoTruco` double NOT NULL,
  `quandoRetruco` double NOT NULL,
  `quandoValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesquandotruco`
--

LOCK TABLES `centroidesquandotruco` WRITE;
/*!40000 ALTER TABLE `centroidesquandotruco` DISABLE KEYS */;
INSERT INTO `centroidesquandotruco` VALUES (1,1.113636,1.772727,2.5,5,57),(2,2.117647,2.941176,3,5,58),(3,0.427957,0.036559,0.008602,5,59),(4,1.860294,2.639706,0,5,60),(5,2.408333,0,0,5,61);
/*!40000 ALTER TABLE `centroidesquandotruco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesquemganhouenvidoagenteemao`
--

DROP TABLE IF EXISTS `centroidesquemganhouenvidoagenteemao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesquemganhouenvidoagenteemao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo` double NOT NULL,
  `quemPediuEnvido` double NOT NULL,
  `quemPediuRealEnvido` double NOT NULL,
  `quemPediuFaltaEnvido` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `pontosEnvidoRobo` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=478 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesquemganhouenvidoagenteemao`
--

LOCK TABLES `centroidesquemganhouenvidoagenteemao` WRITE;
/*!40000 ALTER TABLE `centroidesquemganhouenvidoagenteemao` DISABLE KEYS */;
INSERT INTO `centroidesquemganhouenvidoagenteemao` VALUES (473,1,2,2.061381,2.048593,5,4.820972),(474,2,1.993151,1.917808,2.013699,5,24.506849),(475,3,3,2,2,5,22.416667),(476,4,1.032738,2.056548,1.958333,5,26.5),(477,5,3,2,2,5,4.7375);
/*!40000 ALTER TABLE `centroidesquemganhouenvidoagenteemao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesquemganhouenvidoagentepe`
--

DROP TABLE IF EXISTS `centroidesquemganhouenvidoagentepe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesquemganhouenvidoagentepe` (
  `grupo` int(11) NOT NULL,
  `quemPediuEnvido` double NOT NULL,
  `quemPediuRealEnvido` double NOT NULL,
  `quemFaltaEnvido` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quemPediuFaltaEnvido` double DEFAULT NULL,
  `pontosEnvidoRobo` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=417 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesquemganhouenvidoagentepe`
--

LOCK TABLES `centroidesquemganhouenvidoagentepe` WRITE;
/*!40000 ALTER TABLE `centroidesquemganhouenvidoagentepe` DISABLE KEYS */;
INSERT INTO `centroidesquemganhouenvidoagentepe` VALUES (1,3,1,3,12,405,NULL,27),(2,1,2,2,12,406,NULL,5.172414),(3,1.90625,3,2,12,407,NULL,11.84375),(4,1,2,2,12,408,NULL,30.157895),(5,3,1,3,12,409,NULL,30),(6,2,2,3,12,410,NULL,5.333333),(7,1,2,2,12,411,NULL,24.133858),(8,2.684211,1,2,12,412,NULL,28.710526),(9,2.2,2.066667,1,12,413,NULL,29.2),(10,2,2,2,12,414,NULL,23.921429),(11,3,2,2,12,415,NULL,24.17284),(12,2.346821,2,2,12,416,NULL,5.094412);
/*!40000 ALTER TABLE `centroidesquemganhouenvidoagentepe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesquemtruco`
--

DROP TABLE IF EXISTS `centroidesquemtruco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesquemtruco` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `quemNegouTruco` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesquemtruco`
--

LOCK TABLES `centroidesquemtruco` WRITE;
/*!40000 ALTER TABLE `centroidesquemtruco` DISABLE KEYS */;
INSERT INTO `centroidesquemtruco` VALUES (1,1.50011,0,0,1.49989,9,95),(2,0,0,0,0,9,96),(3,2,0.307328,0,0,9,97),(4,2,1,0,2,9,98),(5,1,0,0,0,9,99),(6,0.967901,1.985185,0.069136,1.083951,9,100),(7,2,1,1.965517,1,9,101),(8,1,2,0.373791,0,9,102),(9,2,1,2,0,9,103);
/*!40000 ALTER TABLE `centroidesquemtruco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesquemtrucoprimeiramao`
--

DROP TABLE IF EXISTS `centroidesquemtrucoprimeiramao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesquemtrucoprimeiramao` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesquemtrucoprimeiramao`
--

LOCK TABLES `centroidesquemtrucoprimeiramao` WRITE;
/*!40000 ALTER TABLE `centroidesquemtrucoprimeiramao` DISABLE KEYS */;
INSERT INTO `centroidesquemtrucoprimeiramao` VALUES (1,3,1,3,7,69,23.878049,10.804878,4.268293),(2,3,1.835938,2,7,70,14.683594,6.011719,3.671875),(3,1,3,1,7,71,30.575,12.8,4.325),(4,1.553525,2.120104,2,7,72,12.697128,5.475196,3.167102),(5,1.242188,2.078125,2,7,73,27.28125,14.554688,6.382812),(6,1,2.153846,1.846154,7,74,44.692308,21.615385,17.538462),(7,1.833333,2.013889,2,7,75,46.902778,6.465278,3.555556);
/*!40000 ALTER TABLE `centroidesquemtrucoprimeiramao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesquemtrucoprimeirape`
--

DROP TABLE IF EXISTS `centroidesquemtrucoprimeirape`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesquemtrucoprimeirape` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesquemtrucoprimeirape`
--

LOCK TABLES `centroidesquemtrucoprimeirape` WRITE;
/*!40000 ALTER TABLE `centroidesquemtrucoprimeirape` DISABLE KEYS */;
INSERT INTO `centroidesquemtrucoprimeirape` VALUES (1,1.085714,2.157143,1.8,7,136,38.728571,23.614286,11.042857),(2,2,2,2,7,137,13.082051,5.492308,3),(3,2.514706,2,2,7,138,17.470588,7.735294,7),(4,1,2.205534,1.924901,7,139,15.197628,7.181818,3.837945),(5,3,1,2.339286,7,140,25.276786,8.205357,4.1875),(6,3,2,2,7,141,13.747826,5.304348,3),(7,1.487805,2.176829,1.902439,7,142,46.243902,8.359756,4.097561);
/*!40000 ALTER TABLE `centroidesquemtrucoprimeirape` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesquemtrucosegundaganhouanterior`
--

DROP TABLE IF EXISTS `centroidesquemtrucosegundaganhouanterior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesquemtrucosegundaganhouanterior` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesquemtrucosegundaganhouanterior`
--

LOCK TABLES `centroidesquemtrucosegundaganhouanterior` WRITE;
/*!40000 ALTER TABLE `centroidesquemtrucosegundaganhouanterior` DISABLE KEYS */;
INSERT INTO `centroidesquemtrucosegundaganhouanterior` VALUES (1,1,3,1,12,234,44.545455,32.363636,13.090909),(2,1.170455,2.079545,2,12,235,46,7.556818,4.863636),(3,3,1,2.247706,12,236,25.715596,9.110092,4.247706),(4,2.6,2,2,12,237,44.818182,7.127273,3.436364),(5,1.222222,1.888889,2,12,238,48.777778,44.333333,5.666667),(6,1,3,1,12,239,35.478261,12.456522,4.847826),(7,1.235294,1.882353,2,12,240,40.176471,20,20),(8,1.034483,2,2,12,241,33.172414,19.632184,6.321839),(9,1,2,2,12,242,47.666667,44.333333,20),(10,1,2.112554,2,12,243,16.064935,6.991342,4.056277),(11,3,2,2,12,244,13.926702,5.544503,3.60733),(12,2,2,2,12,245,15.509709,6.242718,3.912621);
/*!40000 ALTER TABLE `centroidesquemtrucosegundaganhouanterior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesquemtrucosegundaperdeuanterior`
--

DROP TABLE IF EXISTS `centroidesquemtrucosegundaperdeuanterior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesquemtrucosegundaperdeuanterior` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesquemtrucosegundaperdeuanterior`
--

LOCK TABLES `centroidesquemtrucosegundaperdeuanterior` WRITE;
/*!40000 ALTER TABLE `centroidesquemtrucosegundaperdeuanterior` DISABLE KEYS */;
INSERT INTO `centroidesquemtrucosegundaperdeuanterior` VALUES (1,3,1,2,12,205,26.05,5.9,3.6),(2,2.604046,2,2,12,206,11.043353,4.569364,3),(3,1,3,1,12,207,33.285714,20,4.142857),(4,3,1,3,12,208,22.764706,9.647059,4.588235),(5,1,3,2,12,209,21.987342,7.329114,3.759494),(6,1,2.5,1.5,12,210,51,41,3),(7,1,2,2,12,211,49,20,16.8),(8,2.06383,2,2,12,212,21.606383,7.957447,7),(9,1.450704,2,2,12,213,12.873239,6.206573,3),(10,1.235294,1.941176,2,12,214,35.882353,20,5.117647),(11,1.844828,2,2,12,215,46.603448,5.853448,3),(12,1,3,1,12,216,20.269231,5.615385,3.307692);
/*!40000 ALTER TABLE `centroidesquemtrucosegundaperdeuanterior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesquemtrucoterceiraganhouanterior`
--

DROP TABLE IF EXISTS `centroidesquemtrucoterceiraganhouanterior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesquemtrucoterceiraganhouanterior` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=228 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesquemtrucoterceiraganhouanterior`
--

LOCK TABLES `centroidesquemtrucoterceiraganhouanterior` WRITE;
/*!40000 ALTER TABLE `centroidesquemtrucoterceiraganhouanterior` DISABLE KEYS */;
INSERT INTO `centroidesquemtrucoterceiraganhouanterior` VALUES (1,3,1.823009,2,14,214,20.486726,6.79646,3),(2,1,3,1,14,215,21.3,5.85,3.4),(3,1.527859,2.140762,2,14,216,15.131965,6.173021,3),(4,3,1,3,14,217,38,15.181818,5.227273),(5,1.52,2.126667,2,14,218,46.4,6.673333,3.266667),(6,1.094118,2,2,14,219,45.941176,15.258824,6.917647),(7,1,2.5,1.5,14,220,47.666667,35.666667,20),(8,3,1,2,14,221,46,20,20),(9,1,3,1,14,222,37.538462,21.615385,4.615385),(10,1,2,2,14,223,40.777778,20,20),(11,3,1.818182,2,14,224,25.363636,8.563636,6.927273),(12,1.222222,1.888889,2,14,225,48.777778,44.333333,5.666667),(13,1.358209,2.097015,2,14,226,18.455224,11.104478,6.768657),(14,3,1,3,14,227,16.846154,8.076923,4.846154);
/*!40000 ALTER TABLE `centroidesquemtrucoterceiraganhouanterior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesquemtrucoterceiraperdeuanterior`
--

DROP TABLE IF EXISTS `centroidesquemtrucoterceiraperdeuanterior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesquemtrucoterceiraperdeuanterior` (
  `grupo` int(11) NOT NULL,
  `quemTruco` double NOT NULL,
  `quemRetruco` double NOT NULL,
  `quemValeQuatro` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartaAltaRoboClustering` double NOT NULL,
  `cartaMediaRoboClustering` double NOT NULL,
  `cartaBaixaRoboClustering` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesquemtrucoterceiraperdeuanterior`
--

LOCK TABLES `centroidesquemtrucoterceiraperdeuanterior` WRITE;
/*!40000 ALTER TABLE `centroidesquemtrucoterceiraperdeuanterior` DISABLE KEYS */;
INSERT INTO `centroidesquemtrucoterceiraperdeuanterior` VALUES (1,3,2,2,8,143,12.475862,5.086207,3),(2,1.74359,2.034188,2,8,144,20.512821,8.905983,7.042735),(3,3,1,3,8,145,22.8,9.066667,4.066667),(4,2,2,2,8,146,10.178404,4.408451,3),(5,1.987952,2.048193,2,8,147,45.698795,6.60241,3),(6,1,2.982143,1.017857,8,148,34.410714,14.607143,5.357143),(7,1.010753,2.129032,2,8,149,13.580645,6.553763,3),(8,3,1,2,8,150,21.894737,7.305263,3.842105);
/*!40000 ALTER TABLE `centroidesquemtrucoterceiraperdeuanterior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidessegundacartaroboganhouaprimeira`
--

DROP TABLE IF EXISTS `centroidessegundacartaroboganhouaprimeira`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidessegundacartaroboganhouaprimeira` (
  `grupo` int(11) NOT NULL,
  `segundaCartaRoboClustering` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidessegundacartaroboganhouaprimeira`
--

LOCK TABLES `centroidessegundacartaroboganhouaprimeira` WRITE;
/*!40000 ALTER TABLE `centroidessegundacartaroboganhouaprimeira` DISABLE KEYS */;
INSERT INTO `centroidessegundacartaroboganhouaprimeira` VALUES (1,-5,3,132),(2,11.545171,3,133),(3,-46,3,134);
/*!40000 ALTER TABLE `centroidessegundacartaroboganhouaprimeira` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidessegundacartaroboperdeuaprimeira`
--

DROP TABLE IF EXISTS `centroidessegundacartaroboperdeuaprimeira`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidessegundacartaroboperdeuaprimeira` (
  `grupo` int(11) NOT NULL,
  `segundaCartaHumanoClustering` double NOT NULL,
  `segundaCartaRoboClustering` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidessegundacartaroboperdeuaprimeira`
--

LOCK TABLES `centroidessegundacartaroboperdeuaprimeira` WRITE;
/*!40000 ALTER TABLE `centroidessegundacartaroboperdeuaprimeira` DISABLE KEYS */;
INSERT INTO `centroidessegundacartaroboperdeuaprimeira` VALUES (1,10.741313,46,9,301),(2,4.065574,-46,9,302),(3,4.429167,13.75,9,303),(4,41,-15,9,304),(5,12.45283,-14.575472,9,305),(6,-13.394737,-15,9,306),(7,44.333333,46,9,307),(8,49.26087,-4.130435,9,308),(9,-46,-16.69403,9,309);
/*!40000 ALTER TABLE `centroidessegundacartaroboperdeuaprimeira` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesterceiracartaroboganhouasegunda`
--

DROP TABLE IF EXISTS `centroidesterceiracartaroboganhouasegunda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesterceiracartaroboganhouasegunda` (
  `grupo` int(11) NOT NULL,
  `terceiraCartaRoboClustering` double NOT NULL,
  `numerok` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tereiraCartaRoboClustering` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesterceiracartaroboganhouasegunda`
--

LOCK TABLES `centroidesterceiracartaroboganhouasegunda` WRITE;
/*!40000 ALTER TABLE `centroidesterceiracartaroboganhouasegunda` DISABLE KEYS */;
INSERT INTO `centroidesterceiracartaroboganhouasegunda` VALUES (1,-14.826733,4,165,NULL),(2,-46,4,166,NULL),(3,13.47027,4,167,NULL),(4,46,4,168,NULL);
/*!40000 ALTER TABLE `centroidesterceiracartaroboganhouasegunda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centroidesterceiracartaroboperdeuasegunda`
--

DROP TABLE IF EXISTS `centroidesterceiracartaroboperdeuasegunda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centroidesterceiracartaroboperdeuasegunda` (
  `grupo` int(11) NOT NULL,
  `terceiraCartaHumanoClustering` double NOT NULL,
  `terceiraCartaRoboClustering` double NOT NULL,
  `numerok` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centroidesterceiracartaroboperdeuasegunda`
--

LOCK TABLES `centroidesterceiracartaroboperdeuasegunda` WRITE;
/*!40000 ALTER TABLE `centroidesterceiracartaroboperdeuasegunda` DISABLE KEYS */;
INSERT INTO `centroidesterceiracartaroboperdeuasegunda` VALUES (1,-22.653226,-46,3,126),(2,19.564189,29.847973,3,127),(3,-11.036245,-13.445167,3,128);
/*!40000 ALTER TABLE `centroidesterceiracartaroboperdeuasegunda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maos`
--

DROP TABLE IF EXISTS `maos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maos` (
  `idMao` int(11) DEFAULT NULL,
  `idPartida` varchar(255) DEFAULT NULL,
  `jogadorMao` int(11) DEFAULT NULL,
  `cartaAltaRobo` int(11) DEFAULT NULL,
  `cartaMediaRobo` int(11) DEFAULT NULL,
  `cartaBaixaRobo` int(11) DEFAULT NULL,
  `cartaAltaHumano` int(11) DEFAULT NULL,
  `cartaMediaHumano` int(11) DEFAULT NULL,
  `cartaBaixaHumano` int(11) DEFAULT NULL,
  `primeiraCartaRobo` int(11) DEFAULT NULL,
  `primeiraCartaHumano` int(11) DEFAULT NULL,
  `segundaCartaRobo` int(11) DEFAULT NULL,
  `segundaCartaHumano` int(11) DEFAULT NULL,
  `terceiraCartaRobo` int(11) DEFAULT NULL,
  `terceiraCartaHumano` int(11) DEFAULT NULL,
  `ganhadorPrimeiraRodada` int(11) DEFAULT NULL,
  `ganhadorSegundaRodada` int(11) DEFAULT NULL,
  `ganhadorTerceiraRodada` int(11) DEFAULT NULL,
  `roboCartaVirada` int(11) DEFAULT NULL,
  `humanoCartaVirada` int(11) DEFAULT NULL,
  `quemPediuEnvido` int(11) DEFAULT '0',
  `quemPediuFaltaEnvido` int(11) DEFAULT NULL,
  `quemPediuRealEnvido` int(11) DEFAULT NULL,
  `pontosEnvidoRobo` int(11) DEFAULT NULL,
  `pontosEnvidoHumano` int(11) DEFAULT NULL,
  `quemNegouEnvido` int(11) DEFAULT NULL,
  `quemGanhouEnvido` int(11) DEFAULT NULL,
  `tentosEnvido` int(11) DEFAULT NULL,
  `quemFlor` int(11) DEFAULT NULL,
  `quemContraFlor` int(11) DEFAULT NULL,
  `quemContraFlorResto` int(11) DEFAULT NULL,
  `quemNegouFlor` int(11) DEFAULT NULL,
  `pontosFlorRobo` int(11) DEFAULT NULL,
  `pontosFlorHumano` int(11) DEFAULT NULL,
  `quemGanhouFlor` int(11) DEFAULT NULL,
  `tentosFlor` int(11) DEFAULT NULL,
  `quemEscondeuPontosEnvido` int(11) DEFAULT NULL,
  `quemEscondeuPontosFlor` int(11) DEFAULT NULL,
  `quemTruco` int(11) DEFAULT NULL,
  `quandoTruco` int(11) DEFAULT NULL,
  `quemRetruco` int(11) DEFAULT NULL,
  `quandoRetruco` int(11) DEFAULT NULL,
  `quemValeQuatro` int(11) DEFAULT NULL,
  `quandoValeQuatro` int(11) DEFAULT NULL,
  `quemNegouTruco` int(11) DEFAULT NULL,
  `quemGanhouTruco` int(11) DEFAULT NULL,
  `tentosTruco` int(11) DEFAULT NULL,
  `tentosAnterioresRobo` int(11) DEFAULT NULL,
  `tentosAnterioresHumano` int(11) DEFAULT NULL,
  `tentosPosterioresRobo` int(11) DEFAULT NULL,
  `tentosPosterioresHumano` int(11) DEFAULT NULL,
  `roboMentiuEnvido` int(11) DEFAULT NULL,
  `humanoMentiuEnvido` int(11) DEFAULT NULL,
  `roboMentiuFlor` int(11) DEFAULT NULL,
  `humanoMentiuFlor` int(11) DEFAULT NULL,
  `roboMentiuTruco` int(11) DEFAULT NULL,
  `humanoMentiuTruco` int(11) DEFAULT NULL,
  `quemBaralho` int(11) DEFAULT NULL,
  `quandoBaralho` int(11) DEFAULT NULL,
  `quemContraFlorFalta` int(11) DEFAULT NULL,
  `quemEnvidoEnvido` int(11) DEFAULT NULL,
  `quemFlorFlor` int(11) DEFAULT NULL,
  `quandoCartaVirada` int(11) DEFAULT NULL,
  `hasDeception` int(11) DEFAULT NULL,
  `roboMentiuRound1` int(11) DEFAULT NULL,
  `humanoMentiuRound1` int(11) DEFAULT NULL,
  `roboMentiuRound2` int(11) DEFAULT NULL,
  `humanoMentiuRound2` int(11) DEFAULT NULL,
  `roboMentiuRound3` int(11) DEFAULT NULL,
  `humanoMentiuRound3` int(11) DEFAULT NULL,
  `naipeCartaAltaRobo` varchar(45) DEFAULT NULL,
  `naipeCartaMediaRobo` varchar(45) DEFAULT NULL,
  `naipeCartaBaixaRobo` varchar(45) DEFAULT NULL,
  `naipeCartaAltaHumano` varchar(45) DEFAULT NULL,
  `naipeCartaMediaHumano` varchar(45) DEFAULT NULL,
  `naipeCartaBaixaHumano` varchar(45) DEFAULT NULL,
  `naipePrimeiraCartaRobo` varchar(45) DEFAULT NULL,
  `naipePrimeiraCartaHumano` varchar(45) DEFAULT NULL,
  `naipeSegundaCartaRobo` varchar(45) DEFAULT NULL,
  `naipeSegundaCartaHumano` varchar(45) DEFAULT NULL,
  `naipeTerceiraCartaRobo` varchar(45) DEFAULT NULL,
  `naipeTerceiraCartaHumano` varchar(45) DEFAULT NULL,
  `quantidadeChamadasHumano` int(11) DEFAULT '0',
  `quantidadeChamadasRobo` int(11) DEFAULT '0',
  `qualidadeMaoRobo` int(11) DEFAULT '0',
  `qualidadeMaoHumano` int(11) DEFAULT '0',
  `primeiraCartaHumanoClustering` int(11) DEFAULT NULL,
  `segundaCartaHumanoClustering` int(11) DEFAULT NULL,
  `terceiraCartaHumanoClustering` int(11) DEFAULT NULL,
  `primeiraCartaRoboClustering` int(11) DEFAULT NULL,
  `segundaCartaRoboClustering` int(11) DEFAULT NULL,
  `terceiraCartaRoboClustering` int(11) DEFAULT NULL,
  `quantidadeChamadasEnvidoRobo` int(11) DEFAULT NULL,
  `quantidadeChamadasEnvidoHumano` int(11) DEFAULT NULL,
  `cartaAltaRoboClustering` int(11) DEFAULT NULL,
  `cartaMediaRoboClustering` int(11) DEFAULT NULL,
  `cartaBaixaRoboClustering` int(11) DEFAULT NULL,
  `clusterPrimeiraCartaAgentePe` int(11) DEFAULT NULL,
  `clusterSegundaCartaAgenteGanhouAprimeira` int(11) DEFAULT NULL,
  `clusterSegundaCartaAgentePerdeuAprimeira` int(11) DEFAULT NULL,
  `clusterTerceiraCartaAgenteGanhouAsegunda` int(11) DEFAULT NULL,
  `clusterGanhadorUltimaRodada` int(11) DEFAULT NULL,
  `clusterTerceiraCartaAgentePerdeuAsegunda` int(11) DEFAULT NULL,
  `clusterPrimeiraCartaAgenteMao` int(11) DEFAULT NULL,
  `ganhadorMao` int(11) DEFAULT NULL,
  `clusterQuemEnvidoAgenteMao` int(11) DEFAULT NULL,
  `clusterQuemFlor` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `saldoTruco` int(11) DEFAULT '0',
  `saldoEnvido` int(11) DEFAULT NULL,
  `saldoFlor` int(11) DEFAULT '0',
  `clusterQuemEnvidoAgentePe` int(11) DEFAULT '0',
  `clusteringindexacao` int(11) DEFAULT '0',
  `clusterBaralho` int(11) DEFAULT '0',
  `clusteringIndexacaoPontos` int(11) DEFAULT '0',
  `casoUtilTruco` int(11) DEFAULT '0',
  `casoUtilEnvido` int(11) DEFAULT '0',
  `casoUtilContraFlor` int(11) DEFAULT '0',
  `quemPediuEnvidoCluster` int(11) DEFAULT '2',
  `quemPediuRealEnvidoCluster` int(11) DEFAULT NULL,
  `quemPediuFaltaEnvidoCluster` int(11) DEFAULT NULL,
  `quemNegouEnvidoCluster` int(11) DEFAULT NULL,
  `quemTrucoCluster` int(11) DEFAULT NULL,
  `quemRetrucoCluster` int(11) DEFAULT NULL,
  `quemValeQuatroCluster` int(11) DEFAULT NULL,
  `quemNegouTrucoCluster` int(11) DEFAULT NULL,
  `utilTruco` int(11) DEFAULT '0',
  `utilEnvido` int(11) DEFAULT '0',
  `utilFlor` int(11) DEFAULT '0',
  `utilCarta` int(11) DEFAULT '0',
  `quemPediuFlorCluster` int(11) DEFAULT '0',
  `quemPediuContraFlorCluster` int(11) DEFAULT '0',
  `quemPediuContraFlorEoRestoCluster` int(11) DEFAULT '0',
  `diferencaPontuacao` int(11) DEFAULT '0',
  `idOrigem` int(11) DEFAULT NULL,
  `clusterQuemTrucoPrimeiraMao` int(11) DEFAULT '0',
  `clusterQuemTrucoPrimeiraPe` int(11) DEFAULT '0',
  `clusterQuemTrucoSegundaGanhouAnterior` int(11) DEFAULT '0',
  `clusterQuemTrucoSegundaPerdeuAnterior` int(11) DEFAULT '0',
  `clusterQuemTrucoTerceiraGanhouAnterior` int(11) DEFAULT '0',
  `clusterQuemTrucoTerceiraPerdeuAnterior` int(11) DEFAULT '0',
  `nomePlayer1` varchar(45) DEFAULT NULL,
  `nomePlayer2` varchar(45) DEFAULT NULL,
  `tempoQueOponenteJogaTruco` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=305 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maos`
--

LOCK TABLES `maos` WRITE;
/*!40000 ALTER TABLE `maos` DISABLE KEYS */;
/*!40000 ALTER TABLE `maos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matches`
--

DROP TABLE IF EXISTS `matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matches` (
  `idMatch` int(11) NOT NULL,
  `player1` int(11) NOT NULL,
  `player2` int(11) NOT NULL,
  `pointsPlayer1` int(11) NOT NULL,
  `pointsPlayer2` int(11) NOT NULL,
  `winner` int(11) NOT NULL,
  PRIMARY KEY (`idMatch`),
  KEY `fk_player1_idx` (`player1`),
  KEY `fk_player2_idx` (`player2`),
  KEY `fk_winner_idx` (`winner`),
  CONSTRAINT `fk_player1` FOREIGN KEY (`player1`) REFERENCES `players` (`idPlayer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_player2` FOREIGN KEY (`player2`) REFERENCES `players` (`idPlayer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_winner` FOREIGN KEY (`winner`) REFERENCES `players` (`idPlayer`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matches`
--

LOCK TABLES `matches` WRITE;
/*!40000 ALTER TABLE `matches` DISABLE KEYS */;
/*!40000 ALTER TABLE `matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players` (
  `idPlayer` int(11) NOT NULL,
  `extraClusterReuse` varchar(255) NOT NULL,
  `intraClusterReuse` varchar(255) NOT NULL,
  `indCluster` int(11) NOT NULL,
  `caseBase` varchar(255) NOT NULL,
  PRIMARY KEY (`idPlayer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players`
--

LOCK TABLES `players` WRITE;
/*!40000 ALTER TABLE `players` DISABLE KEYS */;
INSERT INTO `players` VALUES (1,'probabilidadechance','probabilidadechance',1,'baseline'),(2,'probabilidadechance','probabilidadechance',0,'baseline'),(3,'chancesucesso','chancesucesso',1,'baseline'),(4,'chancesucesso','chancesucesso',0,'baseline'),(5,'maioria','maioria',1,'baseline'),(6,'maioria','maioria',0,'baseline'),(7,'probabilidadesorteio','probabilidadesorteio',1,'baseline'),(8,'probabilidadesorteio','probabilidadesorteio',0,'baseline'),(9,'chancesucesso','maioria',1,'baseline'),(10,'chancesucesso','probabilidadesorteio',1,'baseline'),(11,'chancesucesso','probabilidadechance',1,'baseline'),(12,'maioria','chancesucesso',1,'baseline'),(13,'maioria','probabilidadechance',1,'baseline'),(14,'maioria','probabilidadesorteio',1,'baseline'),(15,'probabilidadesorteio','chancesucesso',1,'baseline'),(16,'probabilidadesorteio','maioria',1,'baseline'),(17,'probabilidadesorteio','probabilidadechance',1,'baseline'),(18,'probabilidadechance','chancesucesso',1,'baseline'),(19,'probabilidadechance','maioria',1,'baseline'),(20,'probabilidadechance','probabilidadesorteio',1,'baseline'),(21,'chancesucesso','chancesucesso',1,'imitacao'),(22,'probabilidadechance','chancesucesso',1,'imitacao'),(23,'probabilidadechance','probabilidadechance',1,'imitacao'),(24,'maioria','chancesucesso',1,'imitacao'),(25,'probabilidadechance','maioria',1,'imitacao'),(26,'probabilidadechance','probabilidadechance',0,'imitacao'),(27,'chancesucesso','probabilidadechance',1,'imitacao'),(28,'chancesucesso','chancesucesso',0,'imitacao'),(29,'probabilidadesorteio','chancesucesso',1,'imitacao'),(30,'probabilidadechance','probabilidadesorteio',1,'imitacao');
/*!40000 ALTER TABLE `players` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-05 10:36:59
