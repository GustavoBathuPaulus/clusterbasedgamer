#conex?o com o banco de dados

library(DBI)
library(RMySQL)





executeSelectMysql <-function(sql, base){
  m <- dbDriver("MySQL", fetch.default.rec=10000)
  conexaoTrucoCbr <- dbConnect(MySQL(),
                               user="root", password="MysqlPass",
                               dbname="resultadosFinais", host="127.0.0.1", port=3306)
  
  
  
  consulta  = dbSendQuery(conexaoTrucoCbr,sql)
  resultado =  fetch(consulta, n= -1) 
  dbClearResult(consulta)
  dbDisconnect(conexaoTrucoCbr)
  
  
  
  return (resultado)
  
}

#ajusta a diferença de pontuação quando ganha
setaDiferencaPontuacao <- function(player){
  
  sql <-("select * from matches;")
  
  consulta <- executeSelectMysql(sql, "trucocbr")
  
  
  tamanho <- length(consulta[,1])
  tamanho
  numeroDeItensVector <- seq_along(consulta$idMatch)
  m <- dbDriver("MySQL", fetch.default.rec=10000)
  con <- dbConnect(MySQL(),
                   user="root", password="MysqlPass",
                   dbname="resultadosFinais", host="127.0.0.1", port=3306)
  
  
  
  
  for (i in numeroDeItensVector) {
    
    idMatch <- consulta$idMatch[i]
    print(idMatch)
    
    quemEoPlayer1 <- consulta$player1[i]
    quemEoPlayer2 <- consulta$player2[i]
    
    pontosFinaisPlayer1 <-consulta$pointsPlayer1[i]
    pontosFinaisPlayer2 <- consulta$pointsPlayer2[i]
    
    diferencaPontosVencedor <- 0
    diferencaPontosPerdedor <- 0
    #pega a diferença de pontos do vencedor
    if(pontosFinaisPlayer1 == 24){ 
      diferencaPontosVencedor <- 24 - pontosFinaisPlayer2
    } else if(pontosFinaisPlayer2 == 24){
      diferencaPontosVencedor <- 24 - pontosFinaisPlayer1
    }
    
    #pega a diferença de pontos do perdedor
    if(pontosFinaisPlayer1 == 24){ 
      diferencaPontosPerdedor <- pontosFinaisPlayer2 - 24
    } else if(pontosFinaisPlayer2 == 24){
      diferencaPontosPerdedor <- pontosFinaisPlayer1 - 24
    }
    
    sql <-sprintf("UPDATE matches SET diferencaPontosVencedor = %f, diferencaPontosPerdedor = %f  where idMatch = %f;",
                  diferencaPontosVencedor,   diferencaPontosPerdedor, idMatch)
    dbSendQuery(con,sql)  
    
    
    
    
    
  } 
  dbDisconnect(con)
  
}