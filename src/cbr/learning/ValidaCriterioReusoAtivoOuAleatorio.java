package cbr.learning;

import java.util.Collection;
import java.util.List;

import cbr.cbrDescriptions.TrucoDescription;
import jcolibri.method.retrieve.RetrievalResult;

public interface ValidaCriterioReusoAtivoOuAleatorio {
		public boolean aprenderAtivoOuAleatorio(Collection<RetrievalResult> resultRecuperado);
		public boolean aprenderAtivoOuAleatorio(List<TrucoDescription> resultRecuperado);
}
