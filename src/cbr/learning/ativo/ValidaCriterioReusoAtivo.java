package cbr.learning.ativo;

import java.util.Collection;
import java.util.List;

import cbr.cbrDescriptions.TrucoDescription;

import cbr.learning.ValidaCriterioReusoAtivoOuAleatorio;
import jcolibri.method.retrieve.RetrievalResult;

public class ValidaCriterioReusoAtivo implements ValidaCriterioReusoAtivoOuAleatorio {

	@Override
	public boolean aprenderAtivoOuAleatorio(Collection<RetrievalResult> resultRecuperado) {
				
		return resultRecuperado.size() < 10 ? true: false;
	}

	@Override
	public boolean aprenderAtivoOuAleatorio(List<TrucoDescription> resultRecuperado) {
				
		return resultRecuperado.size() < 100? true: false;
	}


	

}
